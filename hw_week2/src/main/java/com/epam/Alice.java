package com.epam;

import java.util.Scanner;

public class Alice {
    public static void main(String[] args) {
        int n = numberOfGuests(), countTimesFullSpreaded = 0, peopleReached = 0, MAX_ITERATION = 10;
        for (int i = 0; i < MAX_ITERATION; i++) {
            boolean guests[] = new boolean[n];
            guests[1] = true;
            boolean alreadyHeard = false;
            int currentGuest = 1, newGuest = -1;
            while (!alreadyHeard) {
                newGuest = 1 + (int) (Math.random() * (n - 1));
                if (newGuest == currentGuest) {
                    while (newGuest == currentGuest)
                        newGuest = 1 + (int) (Math.random() * (n - 1));
                }
                if (guests[newGuest]) {
                    if (rumorSpreaded(guests))
                        countTimesFullSpreaded++;
                    peopleReached += coutnPeopleReached(guests);
                    alreadyHeard = true;
                }
                guests[newGuest] = true;
                currentGuest = newGuest;
            }
        }
        calculation(countTimesFullSpreaded, peopleReached, MAX_ITERATION);
    }
    public static int numberOfGuests() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter number of people at the party: ");
        System.out.println("Remember: Guests have to be more than two.");
        int numOfGuests = sc.nextInt();
        if (numOfGuests <= 2) {
            System.out.println("You entered " + numOfGuests + ". Try again.");
            System.out.println("------------------------------------------");
            numberOfGuests();
        }
        return numOfGuests;
    }

    public static int coutnPeopleReached(boolean arr[]) {
        int pr = 0;
        for (int i = 1; i < arr.length; i++)
            if (arr[i])
                pr++;
        return pr;
    }

    public static boolean rumorSpreaded(boolean arr[]) {
        for (int i = 1; i < arr.length; i++)
            if (!arr[i])
                return false;
        return true;
    }

    public static void calculation(int spreadedTime, int peopleNumbers, int maxIteration) {
        System.out.printf("The probability that everyone will hear rumor except Alice in %s attempts: %s \n",
                maxIteration, (double) spreadedTime / maxIteration);
        System.out.println("Average number of people that heard a rumor " + peopleNumbers / maxIteration);
    }
}
